import React from 'react'

class Loader extends React.Component {

    render() {
        return (
            <div className="bar_loader">
                <div className="ship"></div>
                <p className="p_loader">Carregando</p>
            </div>
        )
    }
}

export default Loader