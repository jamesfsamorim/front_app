import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchBooks } from "../redux/actions/book";
import Loader from "../components/Loader";

class App extends Component {
  constructor(props) {
    super(props)
    this.props.fetchBooks('')
  }

  render() {
    const { books } = this.props

    if(books.ui.loading)
      return <Loader/>

    return (
      <div>
        Hello World !
      </div>
    );
  }
}

const mapStateToProps = state => ({
  books: state
})

const mapActionsToProps = {
  fetchBooks: fetchBooks
}

export default connect(mapStateToProps, mapActionsToProps)(App);
