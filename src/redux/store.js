import {createStore, combineReducers, applyMiddleware, compose} from 'redux';
import { booksReducer } from "../redux/reducers/books.reducer";
import { uiReducer } from "./reducers/ui.reducer";
import { notificationsReducer } from "./reducers/notifications.reducer";
import { booksMiddleware } from "../redux/middleware/books";
import { apiMiddleware } from "../redux/middleware/api";
import { composeWithDevTools } from "redux-devtools-extension";

// shape the state structure
const rootReducer = combineReducers({
    books: booksReducer,
    ui: uiReducer,
    notifications: notificationsReducer
});

// create the feature middleware array
const featureMiddleware = [
    booksMiddleware
];

// create the core middleware array
const coreMiddleware = [
    apiMiddleware
];

// compose the middleware with additional (optional) enhancers,
const enhancer = compose(
    applyMiddleware(...featureMiddleware, ...coreMiddleware),
    composeWithDevTools()
);

export const store = createStore(rootReducer, {}, enhancer )